package com.apoczatek.test.mock;

import java.util.ArrayList;
import java.util.List;

import com.apoczatek.test.mock.example.Book;
import com.apoczatek.test.mock.example.BookService;

//I am a mock object!
public class MockBookServiceImpl implements BookService {

    @Override
    public List<Book> findBookByAuthor(String author) {
        List<Book> books = new ArrayList<>();

        if ("mkyong".equals(author)) {
            books.add(new Book("mkyong in action"));
            books.add(new Book("abc in action"));
            books.add(new Book("bot"));
        }

        return books;
    }

    //implements other methods...

}
