package com.apoczatek.test.mock.example;

public interface BookValidatorService {

    boolean isValid(Book book);

}