package com.apoczatek.test.mock.example;

import java.util.List;

public interface BookService {

    List<Book> findBookByAuthor(String author);

}
