package com.apoczatek.test.mock.example;

public interface AuthorService {

    int getTotalBooks(String author);

}
