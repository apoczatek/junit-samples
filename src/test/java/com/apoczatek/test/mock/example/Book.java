package com.apoczatek.test.mock.example;

public class Book {
	private String name;
	
	public Book(String name) {
		super();
		this.name = name;
	}
	public String getName() {
		return name;
	}
}
