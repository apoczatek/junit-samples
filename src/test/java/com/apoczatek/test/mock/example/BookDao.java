package com.apoczatek.test.mock.example;

import java.util.List;

public interface BookDao {

    List<Book> findBookByAuthor(String author);

}
