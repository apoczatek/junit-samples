package com.apoczatek.test.mock;


import org.junit.Test;

import com.apoczatek.test.mock.example.AuthorServiceImpl;
import com.apoczatek.test.mock.example.BookDaoImpl;
import com.apoczatek.test.mock.example.BookServiceImpl;
import com.apoczatek.test.mock.example.FakeBookValidatorService;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AuthorServiceTest {

	/*
	 * http://www.mkyong.com/unittest/unit-test-what-is-mocking-and-why/
	 * 2. Unit Test - not mock
	 * 
	 * 
To pass above unit test, you need to setup a database in DAO layer, else bookService will return nothing.

2.3 Some disadvantages to perform tests like above :

    This unit test is slow, because you need to start a database in order to get data from DAO.
    This unit test is not isolated, it always depends on external resources like database.
    This unit test can’t ensures the test condition is always the same, the data in the database may vary in time.
    It’s too much work to test a simple method, cause developers skipping the test.

2.4 Solution
The solution is obvious, you need a modified version of the BookServiceImpl class – which will always return the same data for testing, a mock object!

	 * 
	 */
	
    @Test
    public void test_total_book_by_mock() {

		//given
    	//1. Setup
        AuthorServiceImpl obj = new AuthorServiceImpl();
        BookServiceImpl bookService = new BookServiceImpl();
        bookService.setBookDao(new BookDaoImpl()); //Where Dao connect to?
        obj.setBookService(bookService);
        obj.setBookValidatorService(new FakeBookValidatorService());

        //when
		//2. Test method
        int qty = obj.getTotalBooks("mkyong");

        //then
		//3. Verify result
        assertThat(qty, is(2));

    }

}
