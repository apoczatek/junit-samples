package com.apoczatek.test.mock;

import org.junit.Test;

import com.apoczatek.test.mock.example.AuthorServiceImpl;
import com.apoczatek.test.mock.example.FakeBookValidatorService;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AuthorServiceWithMockTest {
 
	/*
	 * http://www.mkyong.com/unittest/unit-test-what-is-mocking-and-why/
	 * 3. Unit Test – Mock Object
	 * 
The above unit test is much better, fast, isolated (no more database) and the test condition (data) is always same.

3.3 But, there are some disadvantages to create mock object manually like above :

    At the end, you may create many mock objects (classes), just for the unit test purpose.
    If the interface contains many methods, you need to override each of them.
    It’s still too much work, and messy!

3.4 Solution
Try Mockito, a simple and powerful mocking framework.
	 
	 */
	
	@Test
    public void test_total_book_by_mock() {

		//1. Setup
        AuthorServiceImpl obj = new AuthorServiceImpl();

        /*BookServiceImpl bookService = new BookServiceImpl();
        bookService.setBookDao(new BookDaoImpl());
        obj.setBookService(bookService);*/

        obj.setBookService(new MockBookServiceImpl());
        obj.setBookValidatorService(new FakeBookValidatorService());

		//2. Test method
        int qty = obj.getTotalBooks("mkyong");

		//3. Verify result
        assertThat(qty, is(2));

    }

}