package com.apoczatek.test;

import org.junit.*;
import static org.hamcrest.MatcherAssert.*;
import org.hamcrest.core.IsNull;
import static org.hamcrest.CoreMatchers.*;

public class NullValueTest {

	/*
	 * http://www.mkyong.com/unittest/hamcrest-how-to-assertthat-check-null-value/?utm_source=mkyong&utm_medium=author&utm_campaign=related-post&utm_content=6
	 */
	
	@Test
	public void app01Test(){
		//not valid
		//assertThat(null, is(null));
	}
	
	
	//1.1 To check null value, try is(nullValue)
	@Test
	public void app02Test()
    {
        //true, check null
        assertThat(null, is(nullValue()));

        //true, check not null
        assertThat("a", is(notNullValue()));
        
      //false, check null
      //assertThat("a", is(nullValue()));
    }
	
	@Test
	public void app03Test()
    {
        //true, check null
        assertThat(null, is(IsNull.nullValue()));

        //true, check not null
        assertThat("a", is(IsNull.notNullValue()));
    }
}
