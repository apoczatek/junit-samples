package com.apoczatek.test.exception;

public class NameNotFoundException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int errCode;

    public NameNotFoundException(int errCode, String message) {
        super(message);
        this.errCode = errCode;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }
}