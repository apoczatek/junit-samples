package com.apoczatek.test.category.test;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.apoczatek.test.category.ClassA;
import com.apoczatek.test.category.ClassB;
import com.apoczatek.test.category.ClassC;
import com.apoczatek.test.category.RegressionTests;

@RunWith(Categories.class)
@Categories.IncludeCategory(RegressionTests.class)
@Suite.SuiteClasses({ClassA.class, ClassB.class, ClassC.class})
public class RegressionTestSuite {
	
	/*
	 * http://www.mkyong.com/unittest/junit-categories-test/
	 * 3.2 Include category example, run RegressionTestSuite category.
	 */
}
