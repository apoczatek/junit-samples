package com.apoczatek.test.category;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ClassC {

	/*
	 * http://www.mkyong.com/unittest/junit-categories-test/
	 * 2. @Category Examples
	 * 2.3 Multiple @Category examples.
	 */
	
    @Category({PerformanceTests.class, RegressionTests.class})
    @Test
    public void test_c_1() {
        assertThat(1 == 1, is(true));
    }

    @Category(RegressionTests.class)
    @Test
    public void test_c_2() {
        assertThat(1 == 1, is(true));
    }

}