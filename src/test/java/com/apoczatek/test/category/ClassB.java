package com.apoczatek.test.category;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@Category({PerformanceTests.class, RegressionTests.class})
public class ClassB {

	/*
	 * http://www.mkyong.com/unittest/junit-categories-test/
	 * 2. @Category Examples
	 * 2.2 @Category on class level.
	 */
	
    @Test
    public void test_b_1() {
        assertThat(1 == 1, is(true));
    }

}