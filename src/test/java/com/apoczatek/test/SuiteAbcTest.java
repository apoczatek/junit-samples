package com.apoczatek.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Exception1Test.class, //test case 1
        TimeoutTest.class     //test case 2
})
public class SuiteAbcTest {
	//normally, this is an empty class
	
	/*
	 * http://www.mkyong.com/unittest/junit-4-tutorial-5-suite-test/
	 */
}
