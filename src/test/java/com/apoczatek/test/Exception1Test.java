package com.apoczatek.test;

import org.junit.Test;
import java.util.ArrayList;

public class Exception1Test {

	/*
	 * http://www.mkyong.com/unittest/junit-4-tutorial-2-expected-exception-test/
	 * 1. @Test expected attribute
	 */
	
    @Test(expected = ArithmeticException.class)
    public void testDivisionWithException() {
        int i = 1 / 0;
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testEmptyList() {
        new ArrayList<>().get(0);
    }

}
